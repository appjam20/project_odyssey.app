import { Storage } from "@capacitor/storage";
import { useState } from "react";
import { STORAGE_APPS_STATE_KEY } from "./consts";

export const storeValue = async (keyName: string, keyValue: string) => {
  return await Storage.set({
    key: keyName,
    value: JSON.stringify(keyValue),
  }).then(() => {
    return true;
  }).catch(error => {
    console.log("Error (storeValue)" + error)
    throw error
  });
};

export const getStoredValue = async (keyName: string) => {
  return await Storage.get({
    key: keyName,
  }).then(response => {
    return response.value != null ? response : null;
  }).catch(error => {
    console.log("Error (retrieveValue)" + error)
    throw error
  });
};
export const storeJsonValue = async <T>(keyName: string, keyValue: T) => {
  return await storeValue(keyName, JSON.stringify(keyValue)
  ).then(() => {
    return true
  }).catch(error => {
    console.log("Error (storeJsonValue)" + error)
    throw error
  });
};

export const getStoredJsonValue = async <T>(keyName: string) => {
  return await getStoredValue(keyName)
    .then(response => {
      return (response != null && response.value != null) ?
        JSON.parse(response.value) as T
        :
        null;
    }).catch(error => {
      console.log("Error (retrieveJsonValue)" + error)
      throw error
    });;
};

export async function storeAppState(state: any){
  await storeJsonValue(STORAGE_APPS_STATE_KEY, state);
}