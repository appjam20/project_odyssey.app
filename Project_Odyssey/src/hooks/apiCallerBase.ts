import { HttpRequest } from "../interfaces/httpInterface";
import { requestAccessToken } from "../oauth/callMsal";
import { IPublicClientApplication } from "@azure/msal-browser";
import { AccountIdentifiers, useMsal } from "@azure/msal-react";

function doNothing() { };

export async function GetRequest(instance: IPublicClientApplication, accounts: AccountIdentifiers[], httpGetRequest: HttpRequest) {

    const accessToken =  await requestAccessToken(instance, accounts);

    //appends the access token to the header of the request
    httpGetRequest.options.headers.entries.length > 0 ? doNothing() : httpGetRequest.options.headers = new Headers();

    httpGetRequest.options.headers.append("Authorization", `Bearer ${accessToken}`);

    return fetch(httpGetRequest.url, httpGetRequest.options)
        .then(response => response.json())
        .catch(error => {
            console.log(error)
            throw error;
        });
}

export async function PostRequest(instance: IPublicClientApplication, accounts: AccountIdentifiers[], httpPostRequest: HttpRequest) {

    const accessToken =  await requestAccessToken(instance, accounts);

    //appends the access token to the header of the request
    httpPostRequest.options.headers.entries.length > 0 ? doNothing() : httpPostRequest.options.headers = new Headers();

    httpPostRequest.options.headers.append("Authorization", `Bearer ${accessToken}`);

    return fetch(httpPostRequest.url, httpPostRequest.options)
        .then(response => response.json())
        .catch(error => {
            console.log(error)
            throw error;
        });
}