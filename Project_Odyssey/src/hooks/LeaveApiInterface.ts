import { HttpRequest } from "../interfaces/httpInterface";
import {  LeaveType, mapLeaveResponseToLeaveType, mapLeaveResponseToLeaveTypes } from "../interfaces/leaveTypes";
import { GetRequest } from "./apiCallerBase";
import { IPublicClientApplication } from "@azure/msal-browser";
import { AccountIdentifiers } from "@azure/msal-react";
import { PROJECT_ODYSSEY_API_LEAVETYPES_GETALL_URL, PROJECT_ODYSSEY_API_LEAVETYPES_GETSPECIFIC_URL, PROJECT_ODYSSEY_API_USERLEAVE_CREATE_URL, PROJECT_ODYSSEY_API_USERLEAVE_GETALLL_URL, PROJECT_ODYSSEY_API_USERLEAVE_GETSPECIFIC_URL, PROJECT_ODYSSEY_API_USERLEAVE_UPDATE_URL } from "../utils/consts";
import { mapAllUserLeaveResponseToUserLeaveList, mapUserLeaveResponseToUserLeave, UserLeave } from "../interfaces/userLeave";

export const GetAllLeaveTypes = async (instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<LeaveType[]> => {

    const httpGetRequest = {
        url: PROJECT_ODYSSEY_API_LEAVETYPES_GETALL_URL,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return mapLeaveResponseToLeaveTypes(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const GetSpecificLeaveType = async (leaveId: string, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<LeaveType> => {
    const httpGetRequest = {
        url: PROJECT_ODYSSEY_API_LEAVETYPES_GETSPECIFIC_URL+"?leaveTypeId="+leaveId,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return mapLeaveResponseToLeaveType(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const GetAllUserLeave = async (emailAddress: string, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserLeave[]> => {
    const httpGetRequest = {
        url: PROJECT_ODYSSEY_API_USERLEAVE_GETALLL_URL+"?EmailAddress="+emailAddress,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return mapAllUserLeaveResponseToUserLeaveList(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const GetUserLeaveItem = async (leaveId: string, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserLeave> => {
    const httpGetRequest = {
        url: PROJECT_ODYSSEY_API_USERLEAVE_GETSPECIFIC_URL+"?userLeaveId="+leaveId,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return mapUserLeaveResponseToUserLeave(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const UpdateUserLeaveItem = async (userLeave: UserLeave, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserLeave> => {
    const httpPutRequest = {
        url: PROJECT_ODYSSEY_API_USERLEAVE_UPDATE_URL,
        options: {
            headers: new Headers(),
            body: userLeave,
            method: "Put"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpPutRequest).then(response => {
        return mapUserLeaveResponseToUserLeave(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const CreateUserLeaveItem = async (userLeave: UserLeave, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<boolean> => {
    const httpPostRequest = {
        url: PROJECT_ODYSSEY_API_USERLEAVE_CREATE_URL,
        options: {
            headers: new Headers(),
            body: userLeave,
            method: "Post"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpPostRequest).then(response => {
        return response;
    }).catch(error => {
        console.log(error)
        throw error
    });
};



