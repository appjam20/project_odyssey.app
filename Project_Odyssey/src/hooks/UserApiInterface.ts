import { graphConfig } from "../oauth/authConfig";
import { HttpRequest } from "../interfaces/httpInterface";
import { mapAPIResponseToUserInfo, mapMSResponseToUserInfo, UserInfo } from "../interfaces/userInfo";
import { GetRequest, PostRequest } from "./apiCallerBase";
import { IPublicClientApplication } from "@azure/msal-browser";
import { AccountIdentifiers } from "@azure/msal-react";
import { PROJECT_ODYSSEY_API_USER_CHECK_EXISTS_URL, PROJECT_ODYSSEY_API_USER_CREATE_URL, PROJECT_ODYSSEY_API_USER_GET_URL, PROJECT_ODYSSEY_API_USER_UPDATE_URL } from "../utils/consts";


export const GetMicrosoftUserDetails = async (instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserInfo> => {

    const httpGetRequest = {
        url: graphConfig.graphMeEndpoint,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return mapMSResponseToUserInfo(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const CheckIfUserExisits = async (userEmail: string, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<boolean> => {
    const httpGetRequest = {
        url: PROJECT_ODYSSEY_API_USER_CHECK_EXISTS_URL + "?EmailAddress=" + userEmail,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return response;
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const GetUserDetails = async (emailAddress: string, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserInfo> => {

    const httpGetRequest = {
        url: PROJECT_ODYSSEY_API_USER_GET_URL + "?EmailAddress=" + emailAddress,
        options: {
            headers: new Headers(),
            method: "Get"
        }
    } as HttpRequest

    return await GetRequest(instance, accounts, httpGetRequest).then(response => {
        return mapAPIResponseToUserInfo(response);
    }).catch(error => {
        console.log(error)
        throw error
    });
};

export const RegisterUser = async (userInfo: UserInfo, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserInfo> => {

    const userExists = await CheckIfUserExisits(userInfo.emailAddress, instance, accounts);

    if (userExists)
        return await GetUserDetails(userInfo.emailAddress, instance, accounts).then(res => { return res });
    else {
        const httpPostRequest = {
            url: PROJECT_ODYSSEY_API_USER_CREATE_URL + "?EmailAddress=" + userInfo.emailAddress,
            options: {
                headers: new Headers(),
                body: userInfo,
                method: "Post"
            }
        } as HttpRequest

        return await PostRequest(instance, accounts, httpPostRequest).then(response => {
            return response;
        }).catch(error => {
            console.log(error)
            throw error
        });
    }
}

export const UpdateUser = async (userInfo: UserInfo, instance: IPublicClientApplication, accounts: AccountIdentifiers[]): Promise<UserInfo> => {
    const httpPutRequest = {
        url: PROJECT_ODYSSEY_API_USER_UPDATE_URL + "?EmailAddress=" + userInfo.emailAddress,
        options: {
            headers: new Headers(),
            body: userInfo,
            method: "Put"
        }
    } as HttpRequest

    return await PostRequest(instance, accounts, httpPutRequest).then(response => {
        return response;
    }).catch(error => {
        console.log(error)
        throw error
    });
}



