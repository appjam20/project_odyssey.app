import React, { createContext, useReducer } from "react";
import { UserInfo } from "../interfaces/userInfo";
import { LeaveType } from "../interfaces/leaveTypes";
import { UserLeave } from "../interfaces/userLeave"
import { STATE_SET_LEAVE_TYPES, STATE_SET_LOADER_STATE, STATE_SET_USER_INFO, STATE_SET_USER_LEAVE } from "../utils/consts";

export type UserInfoState = {
  userInfo: UserInfo,
  astUpdated: Date
  isInit: boolean
};

export type LeaveTypeState = {
  leaveTypes: LeaveType[],
  lastUpdated: Date,
  isInit: boolean
};

export type UserLeaveState = {
  userLeaveItems: UserLeave,
  lastUpdated: Date,
  isInit: boolean
};

export type AppState = {
  userInfoState: UserInfoState,
  leaveTypesState: LeaveTypeState,
  userLeaveState: UserLeaveState,
  showAppLoader: boolean
};

const initialState = {
  userInfoState: {
    userInfo: {
      emailAddress: "",
      contactNumber: "",
      name: "",
      surname: "",
      nickname: "",
      gender: "",
      dateOfBirth: new Date(),
      employmentDate: new Date()
    },
    lastUpdated: new Date(),
    isInit: false
  },
  leaveTypesState: {
    leaveTypes: [{
      typeId: "default",
      type: "default",
      isAdminOnly: false,
      isUnpaid: false,
      isSpecial: false,
      iWorkingDaysOnly: false
    }] as LeaveType[],
    lastUpdated: new Date(),
    isInit: false
  },
  userLeaveState: {
    userLeaveItems: [{
      leaveId: "",
      leaveTypeId: "",
      startDate: new Date(),
      endDate: new Date(),
      isActive: false,
      isApproved: false,
      isPending: false,
      leaveTaken: 0,
      availableLeave: 0,
      leaveExpiryDate: new Date()
    }] as UserLeave[],
    lastUpdated: new Date(),
    isInit: false
  },
  showLoader: false
};

let AppContext = createContext(initialState as any);

let reducer = (state: any, action: any) => {
  switch (action.type) {
    case STATE_SET_USER_INFO: {
      let userInfoStateIn = {
        userInfo: action.userInfo,
        lastUpdated: new Date(),
        isInit: true
      };
      console.log("STATE_SET_USER_INFO");
      console.log(action);
      return { ...state, userInfoState: userInfoStateIn }
    }
    case STATE_SET_LEAVE_TYPES: {
      let leaveTypesStateIn = {
        leaveTypes: action.leaveTypes,
        lastUpdated: new Date(),
        isInit: true
      };
      console.log("STATE_SET_LEAVE_TYPES");
      console.log(action);
      return { ...state, leaveTypesState: leaveTypesStateIn }
    }
    case STATE_SET_USER_LEAVE: {
      let userLeaveStateIn = {
        userLeave: action.userLeave,
        lastUpdated: new Date(),
        isInit: true
      }
      console.log("STATE_SET_USER_LEAVE");
      console.log(action);
      // let value = { ...state, userLeaveState: userLeaveStateIn };
      // console.log(value);
      return { ...state, userLeaveState: userLeaveStateIn }
    }
    case STATE_SET_LOADER_STATE: {
      return { ...state, showLoader: action.showLoader }
    }
  }
  console.log("RETURN STATE");
  console.log(state);
  return state;
};

// create the context provider, we are using use state to ens ure that
// we get reactive values from the context...
function AppContextProvider(props: { children: React.ReactNode; }) {
  //const AppContextProvider = async ({ children }) => {

  //const persistedState = await getStoredJsonValue<UserInfo>("appState");
  console.log(initialState);

  const fullInitialState = {
    ...initialState,
    //...persistedState
  }

  //const { storedValue, setValue } = await useLocalStorage<any>("appState", fullInitialState);

  let [state, dispatch] = useReducer(reducer, fullInitialState);
  let value = { state, dispatch };

  return <AppContext.Provider value={{ state, dispatch }}>{props.children}</AppContext.Provider>;
};

let AppContextConsumer = AppContext.Consumer;

export { AppContext, AppContextProvider, AppContextConsumer };