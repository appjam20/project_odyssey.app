import { IPublicClientApplication, PopupRequest, SilentRequest, } from "@azure/msal-browser";
import { AccountIdentifiers } from "@azure/msal-react";
import { loginRequest } from "./authConfig";

export function handleLogin(instance: IPublicClientApplication) {
    instance.loginRedirect(loginRequest).catch(e => {
        //instance.loginPopup(loginRequest).catch(e => {
        console.error(e);
    });
}

export function handleLogout(instance: IPublicClientApplication) {
    instance.logoutRedirect().catch(e => {
        //instance.logoutPopup().catch(e => {
        console.error(e);
    });
}

export async function requestAccessToken(instance: IPublicClientApplication, accounts: AccountIdentifiers[]) {
    const request = {
        ...loginRequest,
        account: accounts[0]
    };

    // Silently acquires an access token which is then attached to a request for Microsoft Graph data
    return await instance.acquireTokenSilent(request as SilentRequest).then( async (response) => {
        return response.accessToken;
    }).catch((e) => {
        instance.acquireTokenPopup(request as PopupRequest).then( async (response) => {
            return response.accessToken;
        });
    });
}


