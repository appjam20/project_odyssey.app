export type UserInfo = {
    emailAddress: string,
    contactNumber: string,
    name: string,
    surname: string,
    nickname: string,
    gender: string,
    dateOfBirth: Date,
    employmentDate: Date
  };

export const mapMSResponseToUserInfo = (response: any): UserInfo => {
  return {
    emailAddress: response.userPrincipalName,
    contactNumber: "",
    name: response.givenName,
    surname: response.surname,
    nickname: "",
    gender: "",
    dateOfBirth: new Date(),
    employmentDate: new Date()
  };
}

export const mapAPIResponseToUserInfo = (response: any): UserInfo => {
  return {
    emailAddress: response.EmailAddress,
    contactNumber: response.ContactNumber,
    name: response.givenName,
    surname: response.surname,
    nickname: response.Nickname,
    gender: response.Gender,
    dateOfBirth: response.DateOfBirth,
    employmentDate: response.DateOfEmployment
  };
}
