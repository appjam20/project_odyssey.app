export interface HttpRequest{
    url: string,
    options: {
        headers: Headers,
        method: string
    }
};