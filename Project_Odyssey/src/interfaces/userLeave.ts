export interface UserLeave {
    leaveId: string,
    leaveTypeId: string,
    startDate: Date,
    endDate: Date,
    isActive: boolean,
    isApproved: boolean,
    isPending: boolean,
    leaveTaken: number,
    availableLeave: number,
    leaveExpiryDate: Date
};

export const mapAllUserLeaveResponseToUserLeaveList = (response: any): UserLeave[] => {
    let userLeave:UserLeave[] = [];
    (response as Array<any>).forEach(element => {
        userLeave.push(mapUserLeaveResponseToUserLeave(element));
    });
    return userLeave;
};

export const mapUserLeaveResponseToUserLeave = (response: any): UserLeave => {
    return {
        leaveId: response.Id,
        leaveTypeId: response.LeaveType_Id,
        startDate: response.StartDate,
        endDate: response.EndDate,
        isActive: response.IsActive,
        isApproved: response.IsAccepted,
        isPending: response.IsPending,
        leaveTaken: response.Leave_Taken,
        availableLeave: response.Remaining_Leave,
        leaveExpiryDate: response.Leave_Expire
    };
};