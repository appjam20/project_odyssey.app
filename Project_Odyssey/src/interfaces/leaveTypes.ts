export interface LeaveType {
    typeId: string,
    type: string,
    isAdminOnly: boolean,
    isUnpaid: boolean,
    isSpecial: boolean,
    iWorkingDaysOnly: boolean
};

export const mapLeaveResponseToLeaveTypes = (response: any): LeaveType[] => {
    let typesList: LeaveType[] = [];
    (response as Array<any>).forEach(element => {
        typesList.push(mapLeaveResponseToLeaveType(element));
    });
    return typesList;
};

export const mapLeaveResponseToLeaveType = (response: any): LeaveType => {
    return {
        typeId: response.Id,
        type: response.Description,
        isAdminOnly: response.IsAdmin,
        isUnpaid: response.IsUnpaid,
        isSpecial: response.isSpecial,
        iWorkingDaysOnly: response.isSpecial
    }
};