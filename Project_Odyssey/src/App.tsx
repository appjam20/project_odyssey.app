import { IonApp, IonLoading, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Route } from 'react-router-dom';
import Menu from './components/Menu';
import Page from './pages/Page';
import Dashboard from './pages/Dashboard';
import SignInOauth from './pages/SignInOauth';
import UserProfile from './pages/UserProfile';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import { useIsAuthenticated } from '@azure/msal-react';
import React, { useState } from 'react';
import { AppContextProvider } from './states/AppState';
import CardExample from './pages/CardExample';
import Login from './pages/Login';

const App: React.FC = () => {
  const isAuthenticated = useIsAuthenticated();
  const [showLoading, setShowLoading] = useState(false);

  setTimeout(() => {
    setShowLoading(false);
  }, 2000);


  return (
    <IonApp>
      <AppContextProvider>
        <IonReactRouter>
          <IonSplitPane contentId="main">
            <Menu />
            <IonLoading
              cssClass='my-custom-class'
              isOpen={showLoading}
              onDidDismiss={() => setShowLoading(false)}
              message={'Please wait...'}
              duration={5000}
            />
            <IonRouterOutlet id="main">
              <Route
                exact={true}
                path="/"
                render={(props) => {
                  return isAuthenticated ? <Dashboard /> : <SignInOauth />;
                }}
              />
              <Route path="/page/:name" exact={true}>
                <Page />
              </Route>
              <Route path="/dashboard/" exact={true}>
                <Dashboard />
              </Route>
              <Route path="/UserProfile" exact={true}>
                <UserProfile />
              </Route>
              <Route path="/cards" exact={true}>
                <CardExample />
              </Route>
              {/* <Route path="/Login" exact={true}>
                <Login />
              </Route> */}
              {/*<Route path="/SignInOauth" exact={true}>
              <SignInOauth />
            </Route> */}
            </IonRouterOutlet>
          </IonSplitPane>
        </IonReactRouter>
      </AppContextProvider>
    </IonApp>
  );
};

export default App;
