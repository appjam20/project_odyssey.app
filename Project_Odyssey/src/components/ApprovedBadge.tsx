import React, {  } from "react";
import { IonBadge } from "@ionic/react";

const ApprovedBadge: React.FC = () => {
    return (
        <>
            <IonBadge slot="end" color="success">APPROVED</IonBadge>
        </>
    );
};

export default ApprovedBadge;