import { IonHeader, IonToolbar, IonButtons, IonTitle } from '@ionic/react';
import './LeaveMeHeader.css';
import Menu from './Menu';

const LeaveMeHeader = (props:{title:string}) => {
    return (
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <Menu />
                    </IonButtons>
                    <IonTitle>{props.title}</IonTitle>
                </IonToolbar>
            </IonHeader>
          );
};

export default LeaveMeHeader;
