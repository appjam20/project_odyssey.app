import React, { useContext } from "react";
import { IonButton, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonItem, IonLabel } from "@ionic/react";
import { AppContext } from "../states/AppState";
import ApprovedBadge from "./ApprovedBadge";

const ApplyLeaveCardContent: React.FC = () => {
    const { state, dispatch } = useContext(AppContext);


    return (
        <>
            <IonCard>
                <IonCardHeader>
                    <IonCardTitle>Upcoming Leave</IonCardTitle>
                    <IonCardSubtitle>Apply for leave</IonCardSubtitle>
                </IonCardHeader>
                <IonCardContent>
                <IonItem lines="none">
                        <IonLabel ></IonLabel>
                    </IonItem>
                    <IonItem lines="none">
                        <IonLabel>You have no upcoming peave.</IonLabel>
                    </IonItem>
                    <IonItem lines="none"></IonItem>
                </IonCardContent>
            </IonCard>
        </>
    );
};

export default ApplyLeaveCardContent;