import React, { useContext } from "react";
import { IonButton, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCheckbox, IonInput, IonItem, IonItemDivider, IonItemOption, IonItemOptions, IonItemSliding, IonLabel, IonList, IonProgressBar, IonRadio, IonToggle } from "@ionic/react";
import { AppContext } from "../states/AppState";
import ApprovedBadge from "./ApprovedBadge";

const LeaveTypeList: React.FC = () => {
    const { state, dispatch } = useContext(AppContext);
    return (
        <>
            {/* <IonCard class="leaveItemCard" type="button" href="/page/page" onClick={() => { console.log("Card Clicked"); }}>

                <IonCardContent >
                    <IonItem href="/" lines="none" class="leaveTypeItem">
                        <IonLabel>Annual Leave</IonLabel>
                    </IonItem >
                    <IonItem lines="none">
                        <IonProgressBar color="primary" value={0.5}></IonProgressBar>
                        
                    </IonItem>
                </IonCardContent>
            </IonCard> */}
            <IonList>


                <IonItem href="/" lines="none" class="leaveTypeItem">
                    <IonLabel slot="start">Annual Leave</IonLabel>
                    <IonLabel slot="end">10.5/20</IonLabel>
                </IonItem >
                <IonItem>
                    <IonProgressBar color="primary" value={10.5 / 20}></IonProgressBar>
                </IonItem>

                <IonItem href="/" lines="none">
                    <IonLabel slot="start">Sick Leave</IonLabel>
                    <IonLabel slot="end">20/30</IonLabel>
                </IonItem>
                <IonItem>
                    <IonProgressBar color="primary" value={20 / 30}></IonProgressBar>
                </IonItem>

                <IonItem href="/" lines="none">
                    <IonLabel slot="start">Study Leave</IonLabel>
                    <IonLabel slot="end">10/10</IonLabel>
                </IonItem>
                <IonItem>
                    <IonProgressBar color="primary" value={10 / 10}></IonProgressBar>
                </IonItem>

                <IonItem href="/" lines="none">
                    <IonLabel>Special Leave</IonLabel>
                </IonItem>
                <IonItem>
                    <IonProgressBar color="primary" value={15/15}></IonProgressBar>
                </IonItem>

            </IonList>
        </>
    );
};

export default LeaveTypeList;