import React, {  } from "react";
import { useMsal } from "@azure/msal-react";
import { handleLogin } from "../oauth/callMsal";
import { IonButton } from "@ionic/react";


/**
 * Renders a button which, when selected, will redirect the page to the login prompt
 */
const SignInButton: React.FC = () => {
    const { instance } = useMsal();


    return (
        <>
            <IonButton expand="block" className="ml-auto" onClick={() => {handleLogin(instance);}}>SIGN IN</IonButton>
        </>
    );
};

export default SignInButton;