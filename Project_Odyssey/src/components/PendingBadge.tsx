import React, {  } from "react";
import { IonBadge } from "@ionic/react";

const PendingBadge: React.FC = () => {
    return (
        <>
            <IonBadge slot="end" color="warning">PENDING</IonBadge>
        </>
    );
};

export default PendingBadge;