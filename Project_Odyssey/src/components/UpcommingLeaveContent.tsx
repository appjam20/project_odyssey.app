import React, { useContext } from "react";
import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonItem, IonLabel } from "@ionic/react";
import { AppContext } from "../states/AppState";
import ApprovedBadge from "./ApprovedBadge";
import "./UpcommingLeaveContent.css"

const UpcommingLeaveContent: React.FC = () => {
    const { state, dispatch } = useContext(AppContext);
    return (
        <>
            <IonCard type="button" href="/page/page" onClick={() => { console.log("Card Clicked"); }}>
                <IonCardHeader>
                    <IonCardTitle>Upcoming Leave</IonCardTitle>
                    <IonCardSubtitle>Leave Type</IonCardSubtitle>
                </IonCardHeader>

                <IonCardContent>
                    <IonItem lines="none" color="primary">
                        <IonLabel >{"From: "}{state.userLeaveState.userLeave != null ?
                            state.userLeaveState.userLeave[0].startDate.toLocaleDateString()
                            : "  "}</IonLabel>
                    </IonItem>
                    <IonItem lines="none" color="primary">
                        <IonLabel >{"To:   "}{state.userLeaveState.userLeave != null ?
                            state.userLeaveState.userLeave[0].endDate.toLocaleDateString()
                            : "  "}</IonLabel>
                    </IonItem>
                    <IonItem lines="none" color="primary">{"Status:"}<ApprovedBadge /></IonItem>
                </IonCardContent>
            </IonCard>
        </>
    );
};

export default UpcommingLeaveContent;