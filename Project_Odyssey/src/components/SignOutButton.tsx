import React from "react";
import { useMsal } from "@azure/msal-react";
import { handleLogout } from "../oauth/callMsal";
import { IonButton } from "@ionic/react";


/**
 * Renders a button which, when selected, will redirect the page to the login prompt
 */
 const SignOutButton: React.FC = () => {
    const { instance } = useMsal();

    return (
        <IonButton expand="block" className="ml-auto" onClick={() => handleLogout(instance)}>Sign out using Popup</IonButton>
    );
};

export default SignOutButton;