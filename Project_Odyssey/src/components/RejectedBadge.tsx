import React, {  } from "react";
import { IonBadge } from "@ionic/react";

const RejectedBadge: React.FC = () => {
    return (
        <>
            <IonBadge slot="end" color="danger">REJECTED</IonBadge>
        </>
    );
};

export default RejectedBadge;