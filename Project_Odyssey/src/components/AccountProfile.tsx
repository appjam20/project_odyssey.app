import { AuthenticationResult, PopupRequest, SilentRequest } from "@azure/msal-browser";
import { useMsal } from "@azure/msal-react";
import { IonButton } from "@ionic/react";
import { useState } from "react";

const AccountProfile: React.FC = () => {
    const { instance, accounts } = useMsal();

    const name = accounts[0] && accounts[0].username;
    return (
        <>
            {/* {accessToken ? 
                <p>Access Token Acquired!</p>
                :
                <IonButton expand="block" onClick={() => requestAccessToken(instance, accounts)}>Request Access Token</IonButton>
            } */}
        </>
    );
};

export default AccountProfile;