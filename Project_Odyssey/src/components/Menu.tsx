import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
} from '@ionic/react';

import { useLocation } from 'react-router-dom';
import { archiveOutline, archiveSharp, bookmarkOutline, heartOutline, heartSharp, logOutOutline, logOutSharp, mailOutline, mailSharp, paperPlaneOutline, paperPlaneSharp, personOutline, personSharp, trashOutline, trashSharp, warningOutline, warningSharp } from 'ionicons/icons';
import './Menu.css';
import { handleLogout } from '../oauth/callMsal';
import { useMsal } from '@azure/msal-react';
import React, { useContext, useEffect, useState } from 'react';
import { UserInfo } from '../interfaces/userInfo';
import { AppContext } from '../states/AppState';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

interface MenuButton {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
  action: () => void;
}

const appPages: AppPage[] = [
  {
    title: 'DashBoard',
    url: '/page/Inbox',
    iosIcon: "",
    mdIcon: ""
  },
  {
    title: 'Annual Leave',
    url: '/page/Outbox',
    iosIcon: "",
    mdIcon: ""
  },
  {
    title: 'Sick Leave',
    url: '/page/Favorites',
    iosIcon: "",
    mdIcon: ""
  },
  {
    title: 'Study Leave',
    url: '/page/Archived',
    iosIcon: "",
    mdIcon: ""
  },
  {
    title: 'Special Leave',
    url: '/page/Trash',
    iosIcon: "",
    mdIcon: ""
  }
];

const labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

const Menu: React.FC = () => {
  const { state, dispatch } = useContext(AppContext);
  const location = useLocation();
  const { instance } = useMsal();

  const accountButton: MenuButton = {
    url: "/userProfile",
    iosIcon: personOutline,
    mdIcon: personSharp,
    title: "Profile",
    action: () => handleLogout(instance)
  }

  const signOutButton: MenuButton = {
    url: "/",
    iosIcon: logOutOutline,
    mdIcon: logOutSharp,
    title: "Sign out",
    action: () => handleLogout(instance)
  }

  useEffect(() => {
    function updateUserState() {
      console.log();
    };
    updateUserState();
  }, []);

  const menuTitle = "{leave}ME"

  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>{menuTitle}</IonListHeader>
          <IonNote>{state.userInfoState.userInfo.emailAddress}</IonNote>

          <IonMenuToggle key="-2" autoHide={false}>
            <IonItem button onClick={accountButton.action} routerLink={accountButton.url} routerDirection="none" lines="none" detail={false}>
              <IonIcon slot="start" ios={accountButton.iosIcon} md={accountButton.mdIcon} />
              <IonLabel>{accountButton.title}</IonLabel>
            </IonItem>
          </IonMenuToggle>
          <IonMenuToggle key="-1" autoHide={false}>
            <IonItem button onClick={signOutButton.action} routerLink={signOutButton.url} routerDirection="none" lines="none" detail={false}>
              <IonIcon slot="start" ios={signOutButton.iosIcon} md={signOutButton.mdIcon} />
              <IonLabel>{signOutButton.title}</IonLabel>
            </IonItem>
          </IonMenuToggle>
          <IonListHeader>Navigation</IonListHeader>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
