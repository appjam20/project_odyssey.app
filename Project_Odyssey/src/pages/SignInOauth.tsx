import React, { Children, useContext, useState } from "react";
import { useIsAuthenticated } from "@azure/msal-react";
import SignInButton from "../components/SignInButton";
import SignOutButton from "../components/SignOutButton";
import { IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonLoading, IonMenuButton, IonNav, IonPage, IonRow, IonText, IonTitle, IonToolbar, useIonViewDidEnter, useIonViewDidLeave, useIonViewWillEnter, useIonViewWillLeave } from "@ionic/react";
import { AppContext } from "../states/AppState";
import { STATE_SET_LOADER_STATE } from "../utils/consts";

import './SignInOauth.css';

/**
 * Renders the navbar component with a sign-in button if a user is not authenticated
 */
const SignInOauth: React.FC = (props) => {
    //export const SignInOauth: React.FC = (props) => {
    const isAuthenticated = useIsAuthenticated();
    const title = "{leave}ME";
    return (
        <IonPage>
            <IonContent fullscreen>
                <IonGrid>
                    <IonRow>
                        <IonCol>
                            <IonText>
                                <p>{title}</p>
                            </IonText>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            {isAuthenticated ? <SignOutButton /> : <SignInButton />}
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default SignInOauth

