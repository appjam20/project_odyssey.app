import { IonContent, IonHeader, IonPage, IonInput, IonItem, IonLabel, IonGrid, IonRow, IonCol, IonButton, IonRippleEffect, IonText } from '@ionic/react';
import React, { useState } from 'react';
import './Login.css';

const Login: React.FC = () => {

    const loginTitle = "{type}LEAVE";
    //const { name } = useParams<{ name: string; }>();
    const [text, setText] = useState<string>();
    const [number, setNumber] = useState<number>();

    return (
        <IonPage>
            <IonHeader>
            </IonHeader>
            <IonContent>
                <IonGrid>
                    <IonRow id="row1">
                        <IonCol id="col1" >
                            <IonText id="login-title" >{loginTitle}</IonText>
                        </IonCol>
                    </IonRow>
                    <IonRow id="row2">
                        <IonCol id="col2">
                            <IonItem lines="none">
                                <IonLabel
                                    position="floating"
                                    color="light">Email Address</IonLabel>
                                <IonInput
                                    id="email-input"
                                    value={text}
                                    type="email"
                                    autocomplete="email"></IonInput>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                    <IonRow id="row3">
                        <IonCol id="col3">
                            <IonButton
                                className="ion-activatable ripple-parent"
                                expand="block"
                            >SIGN IN
                                <IonRippleEffect type="unbounded"></IonRippleEffect>
                            </IonButton>

                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default Login;
