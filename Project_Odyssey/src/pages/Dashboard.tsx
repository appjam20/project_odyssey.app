import { IonButtons, IonContent, IonFab, IonFabButton, IonHeader, IonIcon, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router';
import { UserInfo } from "../interfaces/userInfo";
import { LeaveType } from "../interfaces/leaveTypes";
import { GetMicrosoftUserDetails } from "../hooks/UserApiInterface";
import { storeAppState } from "../utils/storageHandler";
import { AppContext } from "../states/AppState";
import { STATE_SET_LEAVE_TYPES, STATE_SET_USER_INFO, STATE_SET_USER_LEAVE } from "../utils/consts";

import './Dashboard.css';
import { useIsAuthenticated, useMsal } from '@azure/msal-react';
import UpcommingLeaveContent from '../components/UpcommingLeaveContent';
import ApplyLeaveCardContent from '../components/ApplyLeaveCardContent';
import LeaveTypeList from '../components/LeaveTypeList';

import { UserLeave } from '../interfaces/userLeave';
import { add } from 'ionicons/icons';

const defaultUserInfo = {
    emailAddress: "",
    contactNumber: "",
    name: "",
    surname: "",
    nickname: "",
    gender: "",
    dateOfBirth: new Date(),
    employmentDate: new Date(),
    lastUpdated: new Date()
} as UserInfo;

const defaultLeaveTypes = [{
    typeId: "AnnualLeave",
    type: "AnnualLeave",
    isAdminOnly: false,
    isUnpaid: false,
    isSpecial: false,
    iWorkingDaysOnly: false
}, {
    typeId: "SickLeave",
    type: "SickLeave",
    isAdminOnly: false,
    isUnpaid: false,
    isSpecial: false,
    iWorkingDaysOnly: false
}, {
    typeId: "StudyLeave",
    type: "StudyLeave",
    isAdminOnly: false,
    isUnpaid: false,
    isSpecial: false,
    iWorkingDaysOnly: false
}, {
    typeId: "MaternalLeave",
    type: "MaternalLeave",
    isAdminOnly: false,
    isUnpaid: false,
    isSpecial: false,
    iWorkingDaysOnly: false
}] as LeaveType[];

const defaultUserLeave = {
    leaveId: "leaveID 1",
    leaveTypeId: "AnnualLeave",
    startDate: new Date(),
    endDate: new Date(),
    isActive: true,
    isApproved: true,
    isPending: false,
    leaveTaken: 5,
    availableLeave: 20,
    leaveExpiryDate: new Date()
} as UserLeave;


const Dashboard: React.FC = (props) => {
    const { state, dispatch } = useContext(AppContext);
    const { name } = useParams<{ name: string; }>();
    const { instance, accounts } = useMsal();
    const isAuthenticated = useIsAuthenticated();
    let userInformation = defaultUserInfo;
    let leaveTypesLocal = [] as LeaveType[];
    let userLeaveLocal = [] as UserLeave[];


    async function getLatestMSUserInfo() {
        if ((state.userInfoState != null && !state.userInfoState.isInit)) {
            userInformation = await GetMicrosoftUserDetails(instance, accounts);

            await updateUserInfoState(userInformation);
            return;
        }
    }

    async function getLatestLeaveTypes() {
        if ((state.leaveTypesState != null && !state.leaveTypesState.isInit)) {
            //leaveTypes = await GetAllLeaveTypes(instance, accounts);
            leaveTypesLocal = defaultLeaveTypes;
            await updateLeaveTypesState(leaveTypesLocal);
            return;
        }
    }

    async function getLatestUserLeave() {
        const userLeaveArray: UserLeave[] = [];
        userLeaveArray.push(defaultUserLeave);

        console.log("-+++> ");
        console.log(userLeaveArray);
        if ((state.userLeaveState != null && !state.userLeaveState.isInit)) {
            //userLeave = await GetAllUserLeave(state.userInfoState.userInfo.emailAddress, instance, accounts);

            await updateUserLeaveState(userLeaveArray);
            console.log("+++> ");
            console.log(state.userLeaveState.userLeaveItems);
            return;
        }
    }

    async function updateUserInfoState(userInfoIn: UserInfo) {
        dispatch({
            type: STATE_SET_USER_INFO,
            userInfo: userInfoIn
        });
        await storeAppState(state);
    }

    async function updateLeaveTypesState(leaveTypesIn: LeaveType[]) {
        dispatch({
            type: STATE_SET_LEAVE_TYPES,
            leaveTypes: leaveTypesIn
        });
        await storeAppState(state);
    }

    async function updateUserLeaveState(userLeaveIn: UserLeave[]) {
        console.log("||||||1");
        console.log(userLeaveIn);
        dispatch({
            type: STATE_SET_USER_LEAVE,
            userLeave: userLeaveIn
        });
        //state.userLeaveState.UserLeave = userLeave;
        console.log("||||||2");
        console.log(state);
        await storeAppState(state);
    }

    useEffect(() => {
        async function updateAppState() {
            if (isAuthenticated) {
                await getLatestMSUserInfo();
                // await getLatestUserInfo();
                await getLatestLeaveTypes();
                await getLatestUserLeave();

                console.log("[state]");
                console.log(new Date().getUTCDate());
                console.log(new Date().toLocaleString());
                console.log(new Date().valueOf());
                //console.log(state.userLeaveState.userLeave != null? state.userLeaveState.userLeave[0].startDate.toLocaleDate(): " !  ");
            }
        };
        updateAppState();

    }, [state]);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>{"{leave}ME"}</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                {state.userLeaveState.userLeave != null ?
                    <UpcommingLeaveContent />
                    :
                    <ApplyLeaveCardContent />}

                <LeaveTypeList />
                <IonFab vertical="bottom" horizontal="end"  slot="fixed">
                    <IonFabButton color="danger">
                        <IonIcon icon={add} />
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default Dashboard;
